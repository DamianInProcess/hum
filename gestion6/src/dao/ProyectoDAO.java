package dao;

import java.io.Serializable;
import java.util.List;

import modelos.Desarrollador;

public interface ProyectoDAO<T, Id extends Serializable> {

	public void persist(T entity);

	public void update(T entity);

	public T findById(Id id);

	public T findByName(String nombre);

	public void delete(T entity);

	public List<T> findAll();

	public List<T> findForDesarrollador(Desarrollador desarrollador);

}
