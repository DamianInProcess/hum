package service;

import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import modelos.Desarrollador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import controller.NombresReservados;
import dao.DesarrolladorDAO;

@Service
public class LoginSv implements NombresReservados {

	@Autowired
	private DesarrolladorDAO<Desarrollador, Integer> ds;

	private Desarrollador desarrollador;

	@Transactional
	public boolean Verificar(String principal, String credentials) {
		byte[] salt = new byte[16];
		Base64.Decoder dec = Base64.getDecoder();
		boolean exito = false;

		try {
			desarrollador = ds.findByName(principal);
			salt = dec.decode(desarrollador.getSalt());

			KeySpec spec = new PBEKeySpec(credentials.toCharArray(), salt, 65536, 128);
			SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = f.generateSecret(spec).getEncoded();
			Base64.Encoder enc = Base64.getEncoder();

			credentials = enc.encodeToString(hash);

			if (credentials.equals(desarrollador.getPassword())) {
				exito = true;
			} else {
				throw new BadCredentialsException("Error de login");
			}

		} catch (Exception e) {
			if (trace) e.printStackTrace();
			throw new BadCredentialsException("Error de login");
		}
		return exito;
	}

}
