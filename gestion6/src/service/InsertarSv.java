package service;

import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Random;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import modelos.Desarrollador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import controller.NombresReservados;
import dao.DesarrolladorDAO;

@Service
public class InsertarSv implements NombresReservados {

	@Autowired
	private DesarrolladorDAO<Desarrollador, Integer> ds;

	@Transactional
	public void insertar(Model md, Desarrollador desarrollador) {
		try {
			//
			Random random = new Random();
			byte[] salt = new byte[16];
			random.nextBytes(salt);
			KeySpec spec = new PBEKeySpec(desarrollador.getPassword().toCharArray(),
					salt, 65536, 128);
			SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = f.generateSecret(spec).getEncoded();
			Base64.Encoder enc = Base64.getEncoder();

			desarrollador.setPassword(enc.encodeToString(hash));
			desarrollador.setSalt(enc.encodeToString(salt));
			//
			ds.persist(desarrollador);
			md.addAttribute("exito", true);
		} catch (Exception e) {
			md.addAttribute("desa", new Desarrollador());
			md.addAttribute("fallo", true);
			md.addAttribute("error", "- El nombre '" + desarrollador.getNombre()
					+ "' ya está en uso.");
			if (trace) e.printStackTrace();
		}

	}

}
