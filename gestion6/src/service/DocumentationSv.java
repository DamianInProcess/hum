package service;

import modelos.ImagenProyecto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import controller.NombresReservados;
import dao.ImagenProyectoDAO;

@Service
public class DocumentationSv implements NombresReservados{
	
	@Autowired
	private ImagenProyectoDAO<ImagenProyecto, Integer> imgProDAO;

	@Transactional
	public void initDocumentation(Model md, String ProyectName){
		try{
			md.addAttribute("listaImagenes", imgProDAO.findAll());
		}catch(Exception e){
			if(trace) e.printStackTrace();
		}
	}
	
	
}
