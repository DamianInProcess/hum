package service;

import java.util.ArrayList;
import java.util.List;

import modelos.Desarrollador;
import modelos.Proyecto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import controller.NombresReservados;
import dao.DesarrolladorDAO;
import dao.ProyectoDAO;

@Service
public class MiembrosSv implements NombresReservados {

	@Autowired
	private ProyectoDAO<Proyecto, Integer> proyecto;
	@Autowired
	private DesarrolladorDAO<Desarrollador, Integer> desarrollador;

	private Desarrollador ds;
	private Proyecto pr;

	@Transactional
	public void listaMiembros(Model md, String nombre) {
		try {
			pr = proyecto.findByName(nombre);
			if (pr.getDesarrolladorList1() != null
					&& !pr.getDesarrolladorList().isEmpty()) {
				md.addAttribute("listaMiembros", pr.getDesarrolladorList());
			}
		} catch (Exception e) {
			md.addAttribute("error", "error al listar los miembros del grupo");
			if (trace)
				e.printStackTrace();
		}
	}
	
	@Transactional
	public void listaMiembrosSinCreador(Model md, String nombre) {
		List<Desarrollador> aux = new ArrayList<Desarrollador>();
		try {
			pr = proyecto.findByName(nombre);
			if (pr.getDesarrolladorList1() != null
					&& !pr.getDesarrolladorList().isEmpty()) {
				for (Desarrollador ds : pr.getDesarrolladorList()){
					if(ds.getIddesarrollador() != pr.getIddesarrollador().getIddesarrollador())
						aux.add(ds);
				}
				md.addAttribute("listaMiembros", aux);
			}
		} catch (Exception e) {
			md.addAttribute("error", "error al listar los miembros del grupo");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void agregar(Model md, String nombre) {
		try {
			pr = proyecto.findByName(nombre);
			if (pr.getDesarrolladorList1() != null
					&& !pr.getDesarrolladorList1().isEmpty()) {
				md.addAttribute("listaPendientes", pr.getDesarrolladorList1());
			}
		} catch (Exception e) {
			md.addAttribute("error", true);
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void agregarMiembro(Model md, String nombrePr, String nombre) {
		boolean encontrado = false;
		try {
			ds = desarrollador.findByName(nombre);
			if (ds != null) {
				pr = proyecto.findByName(nombrePr);
				for (Proyecto aux : ds.getProyectoList()) {
					if (aux.getNombre().equals(nombrePr)) {
						encontrado = true;
					}
				}
				if (!encontrado) {
					ds.getProyectoList1().add(pr);
					md.addAttribute("exito", true);
				} else {
					md.addAttribute("error", "El usuario ya forma parte del grupo.");
				}
			} else {
				md.addAttribute("error", "El usuario no existe.");
			}

		} catch (Exception e) {
			md.addAttribute("error",
					"El usuario está pendiente de aceptar tu invitación.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void borrarMiembro(Model md, String nombrePr, String nombre) {
		try {
			ds = desarrollador.findByName(nombre);
			pr = proyecto.findByName(nombrePr);
			if (pr.getIddesarrollador().equals(ds)) {
				md.addAttribute(
						"error",
						"No puedes eliminarte si eres el creador del grupo, para ello deberás borrar el grupo");
			} else {
				ds.getProyectoList().remove(pr);
				md.addAttribute("exito", true);
			}
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido eliminar al usuario.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public boolean validarCreador(Model md, String nombrePr) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		boolean creador = false;
		try {
			pr = proyecto.findByName(nombrePr);
			if (pr.getIddesarrollador().getNombre()
					.compareToIgnoreCase(auth.getName()) == 0) {
				md.addAttribute("creador", creador = true);
			}
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido verificar los datos.");
			if (trace)
				e.printStackTrace();
		}
		return creador;
	}

}
