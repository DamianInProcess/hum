package service;

import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import controller.NombresReservados;
import dao.ComitsDAO;
import dao.DesarrolladorDAO;
import dao.ImagenTareaDAO;
import dao.ProyectoDAO;
import dao.RequisitoDAO;
import dao.TareaDAO;
import modelos.Comits;
import modelos.Desarrollador;
import modelos.ImagenTarea;
import modelos.Proyecto;
import modelos.Requisito;
import modelos.Tarea;

@Service
public class TareaSv implements NombresReservados {

	@Autowired
	private RequisitoDAO<Requisito, Integer> rq;
	@Autowired
	private TareaDAO<Tarea, Integer> tr;
	@Autowired
	private ProyectoDAO<Proyecto, Integer> pr;
	@Autowired
	private DesarrolladorDAO<Desarrollador, Integer> ds;
	@Autowired
	private ImagenTareaDAO<ImagenTarea, Integer> ig;
	@Autowired
	private ComitsDAO<Comits, Integer> cm;

	private Requisito requisito;
	private Tarea tarea;
	private ImagenTarea igTarea;
	private Comits comits;

	@Transactional
	public void agregarTarea(Model md, Tarea tarea, Integer id) {
		try {
			tarea.setVerificado("0");
			requisito = rq.findById(id);
			tarea.setIdrequisito(requisito);
			requisito.getTareaList().add(tarea);
			md.addAttribute("exito", true);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido guardar la tarea.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void borrarTarea(Model md, Integer id) {
		try {
			tarea = tr.findById(id);
			tr.delete(tarea);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido borrar la tarea.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void actualizar(Model md, Integer id) {
		try {
			tarea = tr.findById(id);
			if (tarea != null) {
				md.addAttribute("tareaModelo", tarea);
				md.addAttribute("id", tarea.getIdrequisito().getIdrequisito());
			} else {
				md.addAttribute("error", "No se ha encotrado la Tarea buscado");
			}
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido recueperar la Tarea.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void actualizarTarea(Model md, Tarea tarea, Integer id) {
		try {
			tarea.setIdrequisito(rq.findById(id));
			tr.update(tarea);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido actualizar la Tarea.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void detalles(Model md, Integer id, String nombre) {
		try {
			tarea = tr.findById(id);
			if (tarea != null) {
				if (!tarea.getDesarrolladorList().isEmpty()) {
					for (Desarrollador aux : tarea.getDesarrolladorList()) {
						if (aux.getNombre().compareToIgnoreCase(nombre) == 0) {
							md.addAttribute("participantes", true);
						}
					}
				}
				if (!tarea.getImagenTareaList().isEmpty()) {
					md.addAttribute("listaImagenes", tarea.getImagenTareaList());
				}
				if (!tarea.getComitsList().isEmpty()) {
					md.addAttribute("listaComits", tarea.getComitsList());
				}
				md.addAttribute("tarea", tarea);
				requisito = tarea.getIdrequisito();
				md.addAttribute("requisito", requisito);
				if (pr.findById(requisito.getIdproyecto().getIdproyecto())
						.getIddesarrollador().getNombre().compareToIgnoreCase(nombre) == 0) {
					md.addAttribute("creador", true);
				}
			} else {
				md.addAttribute("error", "No se ha encotrado la Tarea buscado");
			}

		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido recueperar la Tarea.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void detallesComentarios(Model md, Tarea tarOb, Integer idRq) {
		try {
			tarea = tr.findById(tarOb.getIdtarea());
			tarea.setIdrequisito(rq.findById(idRq));
			tarea.setComentarios(tarOb.getComentarios());
			tarea.setVerificado(tarOb.getVerificado());
			tr.update(tarea);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido recueperar la Tarea.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void detallesAbandonar(Model md, Integer id, String nombre) {
		try {
			tarea = tr.findById(id);
			ds.findByName(nombre).getTareaList().remove(tarea);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido recueperar la Tarea.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void detallesParticipar(Model md, Integer id, String nombre) {
		try {
			tarea = tr.findById(id);
			ds.findByName(nombre).getTareaList().add(tarea);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido recueperar la Tarea.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void detallesBorrar(Model md, Integer iddesarrollador, Integer idtarea) {
		try {
			tarea = tr.findById(idtarea);
			ds.findById(iddesarrollador).getTareaList().remove(tarea);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido recueperar la Tarea.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void detallesFotoTareaGuardar(Model md, Integer idtarea,
			MultipartFile pic) {
		ArrayList<Tarea> list = new ArrayList<Tarea>();
		try {
			tarea = tr.findById(idtarea);
			igTarea.setImagen(DatatypeConverter.printBase64Binary(pic.getBytes()));
			igTarea.setNombre(pic.getOriginalFilename());
			list.add(tarea);
			igTarea.setTareaList(list);
			ig.persist(igTarea);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido guardar la Imagen.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void detallesBorrarImagenTarea(Model md, Integer idtarea,
			Integer idImagenTarea) {
		try {
			ig.delete(ig.findById(idImagenTarea));

		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido guardar el comit.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void detallesComitGuardar(Model md, Integer idtarea, String comit) {
		comits = new Comits(); 
		try {
			tarea = tr.findById(idtarea);
			comits.setIdtarea(tarea);
			comits.setComit(comit);
			cm.persist(comits);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido guardar el comit.");
			if (trace)
				e.printStackTrace();
		}
	}
}
