package service;

import modelos.Desarrollador;
import modelos.Proyecto;
import modelos.Requisito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import controller.NombresReservados;
import dao.DesarrolladorDAO;
import dao.ProyectoDAO;

@Service
public class PrincipalSv implements NombresReservados{

	@Autowired
	private DesarrolladorDAO<Desarrollador, Integer> ds;
	@Autowired
	private ProyectoDAO<Proyecto, Integer> pr;

	@Transactional
	public void iniciar(Model md, String nombre, String nombrePr) {
		Proyecto proyecto;
		try {

			proyecto = pr.findByName(nombrePr);

			if (proyecto.getRequisitoList() != null
					&& !proyecto.getRequisitoList().isEmpty()) {
				for (Requisito aux : proyecto.getRequisitoList()) {
					for (int i = 0; i < aux.getTareaList().size(); i++) {
						aux.getTareaList().get(i).getDesarrolladorList().isEmpty();
					}
				}
				md.addAttribute("listaRequisitos", proyecto.getRequisitoList());
			}

			if (proyecto.getIddesarrollador().getNombre().compareToIgnoreCase(nombre) == 0) {
				md.addAttribute("creador", true);
			}

		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido verificar los datos.");
			if(trace) e.printStackTrace();
		}
	}
	
	@Transactional
	public void listaProyectos(Model md, String nombre) {

		Desarrollador desarrollador;
		try {

			desarrollador = ds.findByName(nombre);
			if (!desarrollador.getProyectoList().isEmpty()) {
				md.addAttribute("listaGrupos", desarrollador.getProyectoList());
			}

		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido obtener la lista.");
			if(trace) e.printStackTrace();
		}
	}
}
