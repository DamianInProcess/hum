package service;

import modelos.Desarrollador;
import modelos.Proyecto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import controller.NombresReservados;
import dao.DesarrolladorDAO;
import dao.ProyectoDAO;

@Service
public class PeticionesSv implements NombresReservados {
	
	@Autowired
	private DesarrolladorDAO<Desarrollador, Integer> ds;
	@Autowired
	private ProyectoDAO<Proyecto, Integer> pr;
	
	private Desarrollador desarrollador;
	private Proyecto proyecto;
	
	@Transactional
	public void obtenerPeticiones(Model md, String nombre) {
		try {
			desarrollador = ds.findByName(nombre);
			if (!desarrollador.getProyectoList1().isEmpty()) {
				md.addAttribute("listaPeticiones", desarrollador.getProyectoList1());
			}
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido obtener la lista.");
			if (trace)
				e.printStackTrace();
		} 
	}

	@Transactional
	public void rechazarPeticion(Model md, String nombreProyecto, String nombre) {
		try {
			proyecto = pr.findByName(nombreProyecto);
			desarrollador = ds.findByName(nombre);
			if (!desarrollador.getProyectoList1().isEmpty()) {
				desarrollador.getProyectoList1().remove(proyecto);
			}
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido obtener la lista.");
			if (trace)
				e.printStackTrace();
		} 
	}

	@Transactional
	public void aceptarPeticion(Model md, String nombreProyecto, String nombre) {
		try {
			proyecto = pr.findByName(nombreProyecto);
			desarrollador = ds.findByName(nombre);
			desarrollador.getProyectoList1().remove(proyecto);
			desarrollador.getProyectoList().add(proyecto);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido obtener la lista.");
			if (trace)
				e.printStackTrace();
		}
	}

}
