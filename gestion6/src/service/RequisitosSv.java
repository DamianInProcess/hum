package service;

import modelos.Proyecto;
import modelos.Requisito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import controller.NombresReservados;
import dao.ProyectoDAO;
import dao.RequisitoDAO;
import service.RequisitosSv;

@Service
public class RequisitosSv implements NombresReservados {

	@Autowired
	private ProyectoDAO<Proyecto, Integer> pr;
	@Autowired
	private RequisitoDAO<Requisito, Integer> rq;

	private Requisito requisito;
	private Proyecto proyecto;

	@Transactional
	public void listaRequisitos(Model md, String nombrePr) {
		try {
			proyecto = pr.findByName(nombrePr);
			if (!proyecto.getRequisitoList().isEmpty()) {
				md.addAttribute("listaRequisitos", proyecto.getRequisitoList());
			}
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido obtener la lista.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void agregarRequisito(Model md, Requisito requisito, String nombrePr) {
		try {
			requisito.setValidado("0");
			proyecto = pr.findByName(nombrePr);
			requisito.setIdproyecto(proyecto);
			proyecto.getRequisitoList().add(requisito);
			md.addAttribute("exito", true);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido guardar el requisito.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void borrarRequisito(Model md, Integer id) {
		try {
			requisito = rq.findById(id);
			rq.delete(requisito);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido borrar el requsito.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void actualizar(Model md, Integer id) {
		try {
			requisito = rq.findById(id);
			if (requisito != null) {
				md.addAttribute("requisitoModelo", requisito);
			} else {
				md.addAttribute("error", "No se ha encotrado el requisito buscado");
			}
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido recueperar el requisito.");
			if (trace)
				e.printStackTrace();
		}
	}

	@Transactional
	public void actualizarRequisito(Model md, Requisito reqOb, String nombrePr) {
		try {
			proyecto = pr.findByName(nombrePr);
			reqOb.setIdproyecto(proyecto);
			rq.update(reqOb);
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido actualizar el requisito.");
			if (trace)
				e.printStackTrace();
		}
	}
}
