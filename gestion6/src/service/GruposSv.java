package service;

import modelos.Desarrollador;
import modelos.Proyecto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import controller.NombresReservados;
import dao.DesarrolladorDAO;
import dao.ProyectoDAO;
import service.GruposSv;

@Service
public class GruposSv implements NombresReservados {

	@Autowired
	private DesarrolladorDAO<Desarrollador, Integer> ds;
	@Autowired
	private ProyectoDAO<Proyecto, Integer> pr;

	private Desarrollador desarrollador;
	private Proyecto proyecto;

	@Transactional
	public void listaProyectos(Model md, String nombre) {
		try {
			desarrollador = ds.findByName(nombre);
			if (!desarrollador.getProyectoList().isEmpty()) {
				md.addAttribute("listaGrupos", desarrollador.getProyectoList());
			}

		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido obtener la lista.");
			if(trace) e.printStackTrace();
		}

	}

	@Transactional
	public void seleccionarProyecto(Model md, Integer id) {
		try {
			md.addAttribute("proyecto", pr.findById(id).getNombre());
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido encontrar el proyecto.");
			if(trace) e.printStackTrace();
		}
	}

	@Transactional
	public void verificarCreador(Model md, String proyecto, String usuario) {
		try {
			if (pr.findByName(proyecto).getIddesarrollador().getNombre()
					.equals(usuario)) {
				md.addAttribute("creador", true);
			}
		} catch (Exception e) {
			md.addAttribute("error",
					"No se ha podido verificar el creador del proyecto.");
			if(trace) e.printStackTrace();
		}
	}

	@Transactional
	public void agregarProyectos(Model md, Proyecto proyecto, String nombre) {
		try {
			desarrollador = ds.findByName(nombre);
			proyecto.setIddesarrollador(desarrollador);
			desarrollador.getProyectoList().add(proyecto);
			md.addAttribute("proyecto", proyecto.getNombre());
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido guardar el proyecto.");
			if(trace) e.printStackTrace();
		}

	}

	@Transactional
	public void borrarProyectos(Model md, String nombrePr, String nombre) {
		try {
			proyecto = pr.findByName(nombrePr);
			if (proyecto.getIddesarrollador().getNombre().equals(nombre)) {
				pr.delete(proyecto);
				md.addAttribute("proyecto", vacio);
			} else {
				desarrollador = ds.findByName(nombre);
				desarrollador.getProyectoList().remove(proyecto);
				md.addAttribute("proyecto", vacio);
			}
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido guardar el proyecto.");
			if(trace) e.printStackTrace();
		}
	}

	@Transactional
	public void actualizar(Model md, String nombrePr) {
		try {
			proyecto = pr.findByName(nombrePr);
			if (proyecto != null) {
				md.addAttribute("proyectoModelo", proyecto);
			}
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido guardar el proyecto.");
			if(trace) e.printStackTrace();
		}
	}

	@Transactional
	public void actualizarProyecto(Model md, Proyecto proyOb) {
		try {
			pr.update(proyOb);
			md.addAttribute("proyecto", proyOb.getNombre());
		} catch (Exception e) {
			md.addAttribute("error", "No se ha podido guardar el proyecto.");
			if(trace) e.printStackTrace();
		}
	}
}
