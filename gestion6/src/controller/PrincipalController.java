package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import service.PrincipalSv;

@Controller
@SessionAttributes({ "username", "proyecto" })
public class PrincipalController implements NombresReservados{

	@Autowired
	PrincipalSv principalSv;
	

	@RequestMapping("/principal")
	public String mostrarPrincipal(Model md) {

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		md.addAttribute("username", auth.getName().toUpperCase());
		md.addAttribute("proyecto", vacio);
		principalSv.listaProyectos(md, auth.getName());
		return "principal";
	}

	@RequestMapping("/principal/menu")
	public String regresar(Model md, @ModelAttribute("proyecto") String nombrePr,
			 @ModelAttribute("username") String nombre,
			 @ModelAttribute("aviso") String aviso) {

		if (!nombrePr.equals(vacio)) {
			principalSv.iniciar(md, nombre, nombrePr);
			
		}else{
			principalSv.listaProyectos(md, nombre);
			
			if(md.containsAttribute("aviso")){
				md.addAttribute("aviso", aviso);
			}
		}
		return "principal";
	}

}
