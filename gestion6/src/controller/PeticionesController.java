package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import service.PeticionesSv;

@Controller
@SessionAttributes("username")
public class PeticionesController {

	@Autowired
	PeticionesSv peticionesSv;
	
	@RequestMapping("/peticiones")
	public String obtenerGrupos(Model md,@ModelAttribute("username") String nombre){
		
		peticionesSv.obtenerPeticiones(md, nombre);
		return "peticiones";
	}
	
	
	@RequestMapping("/peticiones/aceptar/{proyectname}")
	public String aceptar(Model md,@PathVariable(value = "proyectname") String proyecto,
			@ModelAttribute("username") String nombre) {
		
		peticionesSv.aceptarPeticion(md, proyecto, nombre);
		return "redirect:/principal/menu" ;
	}
	
	@RequestMapping("/peticiones/rechazar/{proyectname}")
	public String rechazar(Model md,@PathVariable(value = "proyectname") String proyecto,
			@ModelAttribute("username") String nombre) {
		
		peticionesSv.rechazarPeticion(md, proyecto, nombre);
		return "redirect:/principal/menu" ;
	}
}
