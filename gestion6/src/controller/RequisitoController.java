package controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import modelos.Requisito;
import service.RequisitosSv;

@Controller
@SessionAttributes({ "username", "proyecto" })
public class RequisitoController {

	@Autowired
	RequisitosSv requisitosSv;
	

	@RequestMapping("/requisito/agregar")
	public String agregar(Model md) {
		
		md.addAttribute("requisitoModelo", new Requisito());
		return "agregarRequisito";
	}

	@RequestMapping(value = "/requisito/agregar", method = RequestMethod.POST)
	public String agregarRequisito(Model md,
			@ModelAttribute("requisitoModelo") @Valid Requisito requisito,
			BindingResult resultado,@ModelAttribute("proyecto") String nombrePr,
			@ModelAttribute("username") String nombre) {
		
		if (resultado.hasErrors()) {
			md.addAttribute("error", "A sido imposible agregar el requisito.");
			return "agregarRequisito";
		} else {
			requisitosSv.agregarRequisito(md, requisito,nombrePr);
		}

		return "redirect:/principal/menu" ;
	}
	
	
	
	@RequestMapping(value = "/requisito/borrar/{id}")
	public String borrarRequisito(Model md, @PathVariable(value = "id") Integer id,
			@ModelAttribute("username") String nombre) {
		
		requisitosSv.borrarRequisito(md, id);
		return "redirect:/principal/menu";
	}
	
	
	@RequestMapping("/requisito/actualizar/{id}")
	public String actualizar(Model md, @ModelAttribute("username") String nombre,
			@PathVariable(value = "id") Integer id) {
		
		requisitosSv.actualizar(md, id);
		if(md.containsAttribute("requisitoModelo")){
			return "actualizarRequisito";
		}else{
			return "redirect:/principal/menu";
		}
	}
	
	@RequestMapping(value = "/requisito/actualizar", method = RequestMethod.POST)
	public String actualizarRequisito(Model md,@ModelAttribute("requisitoModelo") @Valid Requisito req,
			BindingResult resultado,@ModelAttribute("proyecto") String nombrePr,
			@ModelAttribute("username") String nombre) {
		
		if (resultado.hasErrors()) {
			return "actualizarRequisito";
		}
		requisitosSv.actualizarRequisito(md, req , nombrePr);
		return "redirect:/principal/menu";
	}

}
