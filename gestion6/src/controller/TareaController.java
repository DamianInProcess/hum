 package controller;

import java.util.Date;

import javax.validation.Valid;

import modelos.ImagenTarea;
import modelos.Tarea;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import service.TareaSv;

@Controller
@SessionAttributes({ "username", "proyecto" })
public class TareaController {

	@Autowired
	TareaSv tareaSv;

	@RequestMapping("/tarea/agregar/{id}")
	public String agregar(Model md, @PathVariable(value = "id") Integer id) {

		md.addAttribute("tareaModelo", new Tarea());
		return "agregarTarea";
	}

	@RequestMapping(value = "/tarea/agregar/{id}", method = RequestMethod.POST)
	public String agregarTarea(Model md,
			@ModelAttribute("tareaModelo") @Valid Tarea tarea,
			BindingResult resultado,
			@ModelAttribute("proyecto") String nombrePr,
			@ModelAttribute("username") String nombre,
			@PathVariable(value = "id") Integer id) {

		if (resultado.hasErrors()) {
			md.addAttribute("error", "A sido imposible agregar la tarea.");
			return "agregarTarea";
		} else {
			tareaSv.agregarTarea(md, tarea, id);
		}

		return "redirect:/principal/menu";
	}

	@RequestMapping(value = "/tarea/borrar/{id}")
	public String borrarTarea(Model md, @PathVariable(value = "id") Integer id,
			@ModelAttribute("username") String nombre) {

		tareaSv.borrarTarea(md, id);
		return "redirect:/principal/menu";
	}

	@RequestMapping("/tarea/actualizar/{id}")
	public String actualizar(Model md,
			@ModelAttribute("username") String nombre,
			@PathVariable(value = "id") Integer id) {

		tareaSv.actualizar(md, id);
		if (md.containsAttribute("tareaModelo")) {
			return "actualizarTarea";
		} else {
			return "redirect:/principal/menu";
		}
	}

	@RequestMapping(value = "/tarea/actualizar/{id}", method = RequestMethod.POST)
	public String actualizarTarea(Model md,
			@ModelAttribute("tareaModelo") @Valid Tarea tarea,
			BindingResult resultado,
			@ModelAttribute("proyecto") String nombrePr,
			@ModelAttribute("username") String nombre,
			@PathVariable(value = "id") Integer id) {

		if (resultado.hasErrors()) {
			return "actualizarTarea";
		}
		tareaSv.actualizarTarea(md, tarea, id);
		return "redirect:/principal/menu";
	}

	@RequestMapping("/tarea/detalles/{id}")
	public String detalles(Model md, @ModelAttribute("username") String nombre,
			@PathVariable(value = "id") Integer id) {

		tareaSv.detalles(md, id, nombre);
		if (md.containsAttribute("tarea") && md.containsAttribute("requisito")) {
			md.addAttribute("imagenTarea", new ImagenTarea());
			return "tarea";
		} else {
			return "redirect:/principal/menu";
		}
	}

	@RequestMapping("/tarea/detalles/comentarios/{id}")
	public String detallesComentarios(Model md,
			@ModelAttribute("username") String nombre,
			@PathVariable(value = "id") Integer id,
			@ModelAttribute("tarea") @Valid Tarea tarea) {
		
		tareaSv.detallesComentarios(md, tarea, id);

		return "redirect:/principal/menu";

	}

	@RequestMapping("/tarea/detalles/abandonar/{id}")
	public String detallesAbandonar(Model md,
			@ModelAttribute("username") String nombre,
			@PathVariable(value = "id") Integer id, RedirectAttributes rd) {

		tareaSv.detallesAbandonar(md, id, nombre);
		if (!md.containsAttribute("error")) {
			rd.addFlashAttribute("aviso", "Se te ha desvinculado de la tarea ");
		}
		return "redirect:/principal/menu";

	}

	@RequestMapping("/tarea/detalles/participar/{id}")
	public String detallesParticipar(Model md,
			@ModelAttribute("username") String nombre,
			@PathVariable(value = "id") Integer id, RedirectAttributes rd) {

		tareaSv.detallesParticipar(md, id, nombre);

		return "redirect:/tarea/detalles/" + id;

	}

	@RequestMapping("/tarea/detalles/borrar/{iddesarrollador}/{idtarea}")
	public String detallesBorrar(Model md,
			@ModelAttribute("username") String nombre,
			@PathVariable(value = "iddesarrollador") Integer iddesarrollador,
			@PathVariable(value = "idtarea") Integer idtarea,
			RedirectAttributes rd) {

		tareaSv.detallesBorrar(md, iddesarrollador, idtarea);

		return "redirect:/tarea/detalles/" + idtarea;

	}
	

	@RequestMapping(value = "/tarea/detalles/imagenTarea/{idtarea}", method = RequestMethod.POST)
	public String detallesFotoTarea(Model md,
			@ModelAttribute("username") String nombre,
			@RequestParam(value = "pic") CommonsMultipartFile file,
			@PathVariable(value = "idtarea") Integer idtarea,
			RedirectAttributes rd) {

		if (!file.isEmpty() && file.getSize() > 0){
			tareaSv.detallesFotoTareaGuardar(md, idtarea, file);
		}

		return "redirect:/tarea/detalles/" + idtarea;

	}
	
	@RequestMapping(value = "/tarea/detalles/comit/{idtarea}", method = RequestMethod.POST)
	public String detallesComit(Model md,
			@ModelAttribute("username") String nombre,
			@RequestParam(value = "comit") String comit,
			@PathVariable(value = "idtarea") Integer idtarea,
			RedirectAttributes rd) {

		if (!comit.isEmpty()){
			Date fecha = new Date();
			comit = fecha +" "+nombre+ ": " +comit;
			tareaSv.detallesComitGuardar(md, idtarea, comit);
		}

		return "redirect:/tarea/detalles/" + idtarea;

	}
	
	
	@RequestMapping(value = "/tarea/detalles/borrarImagen/{idimagentarea}/{idtarea}")
	public String detallesBorrarImagen(Model md,
			@ModelAttribute("username") String nombre,
			@PathVariable(value = "idimagentarea") Integer idimagentarea,
			@PathVariable(value = "idtarea") Integer idtarea,
			RedirectAttributes rd) {

			tareaSv.detallesBorrarImagenTarea(md, idtarea, idimagentarea);

		return "redirect:/tarea/detalles/" + idtarea;

	}

}
