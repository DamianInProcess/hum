package controller;

import javax.validation.Valid;

import modelos.Desarrollador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import service.InsertarSv;

@Controller
public class LoginController{

	@Autowired
	private InsertarSv insertarSv;
	
	@RequestMapping("/")
	public String loginShow(Model md) {
		md.addAttribute("desa", new Desarrollador());
		return "login";
	}

	@RequestMapping("/login")
	public String login(Model md) {
		md.addAttribute("desa", new Desarrollador());
		return "login";
	}


	@RequestMapping("/logout")
	public String logout() {
		return "/";
	}

	@RequestMapping(value = "desarrollador", method = RequestMethod.POST)
	public String insertar(@ModelAttribute("desa") @Valid Desarrollador dsa,
			BindingResult resultado, Model md) {

		if (resultado.hasErrors()) {
			md.addAttribute("fallo", true);
			return "login";
		}

		this.insertarSv.insertar(md, dsa);

		return "login";
	}

}

