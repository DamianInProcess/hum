package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import service.MiembrosSv;

@Controller
@SessionAttributes("proyecto")
public class MiembrosController implements NombresReservados {

	@Autowired
	private MiembrosSv miembrosSv;

	@RequestMapping("/miembros")
	public String obtenerMiembros(Model md,
			@ModelAttribute("proyecto") String proyecto,
			RedirectAttributes rd) {

		if (!proyecto.equals(vacio)) {
			miembrosSv.listaMiembros(md, proyecto);
			miembrosSv.validarCreador(md, proyecto);
			return "miembros";
		} else {
			rd.addFlashAttribute("aviso", "Selecciona un grupo.");
			return "redirect:/principal/menu";
		}

	}

	@RequestMapping("/miembros/agregar")
	public String agregar(Model md, @ModelAttribute("proyecto") String proyecto) {
		if (miembrosSv.validarCreador(md, proyecto)) {
			miembrosSv.agregar(md, proyecto);
			return "agregarMiembro";
		} else {
			return "redirect:/principal/menu";
		}

	}

	@RequestMapping(value = "/miembros/agregarMiembro", method = RequestMethod.POST)
	public String agregarMiembro(Model md,
			@ModelAttribute("proyecto") String proyecto,
			@ModelAttribute("nombre") String nombre) {
		if (miembrosSv.validarCreador(md, proyecto)) {
			miembrosSv.agregarMiembro(md, proyecto, nombre);
			miembrosSv.agregar(md, proyecto);
			return "agregarMiembro";
		} else {
			return "redirect:/principal/menu";
		}
	}

	@RequestMapping("/miembros/borrar")
	public String borrar(Model md, @ModelAttribute("proyecto") String proyecto) {
		if (miembrosSv.validarCreador(md, proyecto)) {
			
			miembrosSv.listaMiembrosSinCreador(md, proyecto);
			
			return "borrarMiembro";
		} else {
			return "redirect:/principal/menu";
		}

	}

	@RequestMapping(value = "/miembros/borrar/{user}", method = RequestMethod.POST)
	public String borrarMiembro(Model md,
			@ModelAttribute("proyecto") String proyecto,
			@ModelAttribute("user") String nombre) {
		if (miembrosSv.validarCreador(md, proyecto)) {
			miembrosSv.borrarMiembro(md, proyecto, nombre);
			miembrosSv.listaMiembros(md, proyecto);
			return "redirect:/miembros/borrar";
		} else {
			return "redirect:/principal/menu";
		}
	}

}
