package controller;

import javax.validation.Valid;

import modelos.Proyecto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import service.GruposSv;

@Controller
@SessionAttributes({ "username", "proyecto"})
public class GruposController  implements NombresReservados{

	@Autowired
	private GruposSv gruposSv;

	@RequestMapping("/grupos")
	public String obtenerGrupos(Model md,
			@ModelAttribute("proyecto") String proyecto,
			@ModelAttribute("username") String nombre) {
		
		gruposSv.listaProyectos(md, nombre);
		
		if(!proyecto.equals(vacio)){
			gruposSv.verificarCreador(md, proyecto, nombre);
		}
		return "grupos";
	}

	@RequestMapping(value = "/grupos/{id}", method = RequestMethod.POST)
	public String insertar(Model md,
			@PathVariable(value = "id") Integer id) {
		
		gruposSv.seleccionarProyecto(md, id);
		
		return "redirect:/principal/menu" ;
	}

	@RequestMapping("/grupos/agregar")
	public String agregar(Model md, @ModelAttribute("username") String nombre) {
		
		md.addAttribute("proyectoModelo", new Proyecto());
		return "agregarProyecto";
	}

	@RequestMapping(value = "grupos/agregarProyecto", method = RequestMethod.POST)
	public String agregarGrupo(Model md,
			@ModelAttribute("proyectoModelo") @Valid Proyecto proyecto,
			BindingResult resultado,@ModelAttribute("username") String nombre ) {
		
		if (resultado.hasErrors()) {
			md.addAttribute("error", "Ha sido imposible agregar el proyecto.");
		} else {
			gruposSv.agregarProyectos(md, proyecto,nombre);
		}

		return "redirect:/principal/menu" ;
	}
	
	
	@RequestMapping("/grupos/borrar")
	public String borrar(Model md, @ModelAttribute("username") String nombre) {
		
		gruposSv.listaProyectos(md, nombre);
		return "borrarProyecto";
	}
	
	@RequestMapping(value = "/grupos/borrar/{proyectname}", method = RequestMethod.POST)
	public String borrarGrupo(Model md,
			@PathVariable(value = "proyectname") String proyecto,
			@ModelAttribute("username") String nombre) {
		
		gruposSv.borrarProyectos(md, proyecto, nombre);
		return this.borrar(md, nombre);
	}
	
	
	@RequestMapping("/grupos/actualizar")
	public String actualizar(Model md, @ModelAttribute("username") String nombre,
			@ModelAttribute("proyecto") String proyecto) {
		
		gruposSv.actualizar(md, proyecto);
		if(md.containsAttribute("proyectoModelo")){
			return "actualizarProyecto";
		}else{
			return "redirect:/principal/menu" ;
		}
	}
	
	@RequestMapping(value = "/grupos/actualizar", method = RequestMethod.POST)
	public String actualizarGrupo(Model md,@ModelAttribute("proyectoModelo") @Valid Proyecto proy,
			BindingResult resultado,
			@ModelAttribute("username") String nombre) {
		
		if (resultado.hasErrors()) {
			md.addAttribute("fallo", true);
			return "actualizarProyecto";
		}
		gruposSv.actualizarProyecto(md, proy);
		return this.actualizar(md, nombre, proy.getNombre());
	}
	
}
