package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import service.DocumentationSv;

@Controller
@SessionAttributes({ "username", "proyecto" })
public class DocumentationController {
	
	@Autowired
	DocumentationSv dc;
	
	@RequestMapping("/documentation")
	public String intoDocumentation(Model md,@ModelAttribute("username") String nombre,
			@ModelAttribute("proyecto") String nombrePr){
		dc.initDocumentation(md, nombrePr);
		return "documentation";
	}
	
	
	
}
