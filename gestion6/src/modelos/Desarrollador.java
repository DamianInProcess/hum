package modelos;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "desarrollador")
@NamedQueries({
    @NamedQuery(name = "Desarrollador.findAll", query = "SELECT d FROM Desarrollador d")})
public class Desarrollador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "iddesarrollador")
    private Integer iddesarrollador;
    @NotBlank
    @NotEmpty
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "password")
    @NotEmpty
    @Size(min=6, max=45)
    private String password;
    @Basic(optional = false)
    @Column(name = "salt")
    private String salt;
    @JoinTable(name = "proyecto_desarrollador", joinColumns = {
        @JoinColumn(name = "iddesarrollador", referencedColumnName = "iddesarrollador")}, inverseJoinColumns = {
        @JoinColumn(name = "idproyecto", referencedColumnName = "idproyecto")})
    @ManyToMany(cascade=CascadeType.ALL)
    private List<Proyecto> proyectoList;
    @JoinTable(name = "tarea_desarrollador", joinColumns = {
        @JoinColumn(name = "iddesarrollador", referencedColumnName = "iddesarrollador")}, inverseJoinColumns = {
        @JoinColumn(name = "idtarea", referencedColumnName = "idtarea")})
    @ManyToMany(cascade=CascadeType.ALL)
    private List<Tarea> tareaList;
    @JoinTable(name = "peticiones", joinColumns = {
        @JoinColumn(name = "iddesarrollador", referencedColumnName = "iddesarrollador")}, inverseJoinColumns = {
        @JoinColumn(name = "idproyecto", referencedColumnName = "idproyecto")})
    @ManyToMany(cascade=CascadeType.ALL)
    private List<Proyecto> proyectoList1;
    @OneToMany(mappedBy = "iddesarrollador")
    private List<Proyecto> proyectoList2;

    public Desarrollador() {
    }

    public Desarrollador(Integer iddesarrollador) {
        this.iddesarrollador = iddesarrollador;
    }

    public Desarrollador(Integer iddesarrollador, String nombre, String password, String salt) {
        this.iddesarrollador = iddesarrollador;
        this.nombre = nombre;
        this.password = password;
        this.salt = salt;
    }

    public Integer getIddesarrollador() {
        return iddesarrollador;
    }

    public void setIddesarrollador(Integer iddesarrollador) {
        this.iddesarrollador = iddesarrollador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public List<Proyecto> getProyectoList() {
        return proyectoList;
    }

    public void setProyectoList(List<Proyecto> proyectoList) {
        this.proyectoList = proyectoList;
    }

    
    public List<Tarea> getTareaList() {
        return tareaList;
    }

    public void setTareaList(List<Tarea> tareaList) {
        this.tareaList = tareaList;
    }

    public List<Proyecto> getProyectoList1() {
        return proyectoList1;
    }

    public void setProyectoList1(List<Proyecto> proyectoList1) {
        this.proyectoList1 = proyectoList1;
    }

    public List<Proyecto> getProyectoList2() {
        return proyectoList2;
    }

    public void setProyectoList2(List<Proyecto> proyectoList2) {
        this.proyectoList2 = proyectoList2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddesarrollador != null ? iddesarrollador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Desarrollador)) {
            return false;
        }
        Desarrollador other = (Desarrollador) object;
        if ((this.iddesarrollador == null && other.iddesarrollador != null) || (this.iddesarrollador != null && !this.iddesarrollador.equals(other.iddesarrollador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelos.Desarrollador[ iddesarrollador=" + iddesarrollador + " ]";
    }
    
}
