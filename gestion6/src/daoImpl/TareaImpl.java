package daoImpl;

import dao.TareaDAO;
import modelos.Tarea;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("tareaImpl")
public class TareaImpl implements TareaDAO<Tarea, Integer> {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void persist(Tarea entity) {
		Session session = sessionFactory.getCurrentSession();
		session.save(entity);
	}

	@Override
	public void update(Tarea entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);
	}

	@Override
	public Tarea findById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Query<Tarea> query = session
				.createQuery("FROM modelos.Tarea where id = :id");
		query.setParameter("id", id);
		return (Tarea) query.uniqueResult();
	}

	@Override
	public void delete(Tarea entity) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(entity);
	}

	@Override
	public List<Tarea> findAll() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("FROM modelos.Tarea").list();
	}

	@Override
	public Tarea findByName(String nombre) {
		Session session = sessionFactory.getCurrentSession();
		Query<Tarea> query = session
				.createQuery("FROM modelos.Tarea WHERE nombre = :nombre");
		query.setParameter("nombre", nombre);
		return (Tarea) query.uniqueResult();
	}

}
