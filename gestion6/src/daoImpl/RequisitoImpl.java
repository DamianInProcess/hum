package daoImpl;

import dao.RequisitoDAO;
import modelos.Requisito;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("requisitoImpl")
public class RequisitoImpl implements RequisitoDAO<Requisito, Integer> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void persist(Requisito entity) {
		Session session = sessionFactory.getCurrentSession();
		session.save(entity);
	}

	@Override
	public void update(Requisito entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);
	}

	@Override
	public Requisito findById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Query<Requisito> query = session
				.createQuery("FROM modelos.Requisito where id = :id");
		query.setParameter("id", id);
		return (Requisito) query.uniqueResult();
	}

	@Override
	public void delete(Requisito entity) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(entity);
	}

	@Override
	public List<Requisito> findAll() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("FROM modelos.Requisito").list();
	}

	@Override
	public Requisito findByName(String nombre) {
		Session session = sessionFactory.getCurrentSession();
		Query<Requisito> query = session
				.createQuery("FROM modelos.Requisito WHERE nombre = :nombre");
		query.setParameter("nombre", nombre);
		return (Requisito) query.uniqueResult();
	}

}
