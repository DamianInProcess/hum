package daoImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import modelos.ImagenProyecto;
import dao.ImagenProyectoDAO;

@Component("ImagenProyectoImpl")
public class ImagenProyectoImpl implements
		ImagenProyectoDAO<ImagenProyecto, Integer> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void persist(ImagenProyecto entity) {
		Session session = sessionFactory.getCurrentSession();
		session.save(entity);
	}

	@Override
	public void update(ImagenProyecto entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);
	}

	@Override
	public ImagenProyecto findById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Query<ImagenProyecto> query = null;
		query = session.createQuery("FROM modelos.ImagenProyecto where id = :id");
		query.setParameter("id", id);
		return (ImagenProyecto) query.uniqueResult();
	}

	@Override
	public void delete(ImagenProyecto entity) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(entity);
	}

	@Override
	public List<ImagenProyecto> findAll() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("FROM modelos.ImagenProyecto").list();
	}

}
