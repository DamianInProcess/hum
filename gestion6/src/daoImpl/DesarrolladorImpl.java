package daoImpl;

import dao.DesarrolladorDAO;
import modelos.Desarrollador;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("desarrolladorImpl")
public class DesarrolladorImpl implements
		DesarrolladorDAO<Desarrollador, Integer> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void persist(Desarrollador entity) {
		Session session = sessionFactory.getCurrentSession();
		session.save(entity);
	}

	@Override
	public void update(Desarrollador entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);
	}

	@Override
	public Desarrollador findById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Query<Desarrollador> query = session
				.createQuery("FROM modelos.Desarrollador where id = :id");
		query.setParameter("id", id);
		return (Desarrollador) query.uniqueResult();
	}

	@Override
	public void delete(Desarrollador entity) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(entity);
	}

	@Override
	public List<Desarrollador> findAll() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("FROM modelos.Desarrollador").list();
	}

	@Override
	public Desarrollador findByName(String nombre) {
		Session session = sessionFactory.getCurrentSession();
		Query<?> query = session
				.createQuery("FROM modelos.Desarrollador WHERE nombre = :nombre");
		query.setParameter("nombre", nombre);
		return (Desarrollador) query.uniqueResult();
	}

}
