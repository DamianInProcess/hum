package daoImpl;

import java.util.List;

import modelos.ImagenTarea;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dao.ImagenTareaDAO;

@Component("imagenTareaImpl")
public class ImagenTareaImpl implements ImagenTareaDAO<ImagenTarea, Integer> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void persist(ImagenTarea entity) {
		Session session = sessionFactory.getCurrentSession();
		session.save(entity);
	}

	@Override
	public void update(ImagenTarea entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);
	}

	@Override
	public ImagenTarea findById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Query<ImagenTarea> query = session
				.createQuery("FROM modelos.ImagenTarea where id = :id");
		query.setParameter("id", id);
		return (ImagenTarea) query.uniqueResult();
	}

	@Override
	public void delete(ImagenTarea entity) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(entity);
	}

	@Override
	public List<ImagenTarea> findAll() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("FROM modelos.ImagenTarea").list();
	}

	@Override
	public ImagenTarea findByName(String nombre) {
		Session session = sessionFactory.getCurrentSession();
		Query<ImagenTarea> query = session
				.createQuery("FROM modelos.ImagenTarea WHERE nombre = :nombre");
		query.setParameter("nombre", nombre);
		return (ImagenTarea) query.uniqueResult();
	}

}
