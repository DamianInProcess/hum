package daoImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import modelos.Comits;
import dao.ComitsDAO;

@Component("ComitsImpl")
public class ComitsImpl implements ComitsDAO<Comits, Integer> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void persist(Comits entity) {
		Session session = sessionFactory.getCurrentSession();
		session.save(entity);

	}

	@Override
	public void update(Comits entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);

	}

	@Override
	public Comits findById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Query<Comits> query = session
				.createQuery("FROM modelos.Comits where id = :id");
		query.setParameter("id", id);
		return (Comits) query.uniqueResult();
	}

	@Override
	public void delete(Comits entity) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(entity);

	}

	@Override
	public List<Comits> findAll() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("FROM modelos.Comits").list();
	}

}
