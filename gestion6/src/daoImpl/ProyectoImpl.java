package daoImpl;

import java.util.ArrayList;
import java.util.List;

import modelos.Desarrollador;
import modelos.Proyecto;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dao.ProyectoDAO;

@Component("proyectoImpl")
public class ProyectoImpl implements ProyectoDAO<Proyecto, Integer> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void persist(Proyecto entity) {
		Session session = sessionFactory.getCurrentSession();
		session.save(entity);
	}

	@Override
	public void update(Proyecto entity) {
		Session session = sessionFactory.getCurrentSession();
		Query<?> query = session
				.createQuery("UPDATE modelos.Proyecto set nombre = :nombre,"
						+ " descripcion = :descripcion" + " where idproyecto = :id");
		query.setParameter("nombre", entity.getNombre());
		query.setParameter("descripcion", entity.getDescripcion());
		query.setParameter("id", entity.getIdproyecto());
		query.executeUpdate();
	}

	@Override
	public Proyecto findById(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Query<Proyecto> query = session
				.createQuery("FROM modelos.Proyecto where id = :id");
		query.setParameter("id", id);
		return (Proyecto) query.uniqueResult();
	}

	@Override
	public void delete(Proyecto entity) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("DELETE modelos.Proyecto where id = :id");
		query.setParameter("id", entity.getIdproyecto());
		query.executeUpdate();
	}

	@Override
	public List<Proyecto> findAll() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("FROM modelos.Proyecto").list();
	}

	@Override
	public Proyecto findByName(String nombre) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session
				.createQuery("FROM modelos.Proyecto where nombre = :nombre");
		query.setParameter("nombre", nombre);
		return (Proyecto) query.uniqueResult();
	}

	@Override
	public List<Proyecto> findForDesarrollador(Desarrollador desarrollador) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM modelos.Proyecto");
		List<Proyecto> listaPr = query.list();
		List<Proyecto> listaPrfinal = new ArrayList<Proyecto>();

		// chapuza revisarlo cuando tengas tiempo

		for (Proyecto pr : listaPr) {
			for (Desarrollador ds : pr.getDesarrolladorList()) {
				if (ds.getIddesarrollador().equals(desarrollador.getIddesarrollador())) {
					listaPrfinal.add(pr);
				}
			}
		}

		if (listaPrfinal.isEmpty()) {
			listaPrfinal = null;
		}
		return listaPrfinal;
	}
}
