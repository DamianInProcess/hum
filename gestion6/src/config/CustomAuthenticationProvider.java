package config;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import service.LoginSv;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private LoginSv loginSv;

	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		String principal = authentication.getName();
		String credentials = (String) authentication.getCredentials();
		List<GrantedAuthority> authorities;
		boolean exito = false;
		

		if (!((principal == null) || "".equals(principal))
				|| (credentials == null)) {
			exito = loginSv.Verificar(principal, credentials);
		}

		
		if (exito) {
			authorities = new ArrayList<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
			return new UsernamePasswordAuthenticationToken(principal,
					credentials, authorities);
		} else {
			throw new BadCredentialsException("Error de login");
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}

}
