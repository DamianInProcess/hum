<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>
<body class="container">

  <%@include file="navBar.jsp"%>

  <br>
  <div class="w3-code">
    <h3 style="color: #1171C0;">
      Miembros de: ${proyecto}
      <c:if test="${creador != null}">
        <a href="miembros/agregar" role="button" title="Agregar miembro">
          <span class="glyphicon glyphicon-plus pull-right"
          aria-hidden="true" style="color: #5cb85c;"></span>
        </a>
        <a href="miembros/borrar" role="button" title="Borrar miembro">
          <span class="glyphicon glyphicon-remove pull-right"
          aria-hidden="true" style="color: red;">&nbsp</span>
        </a>
      </c:if>
    </h3>
  </div>
  <br>

  <div>
    <c:choose>

      <c:when test="${listaMiembros == null }">

        <div class="w3-code2">
          <h4>
            <span style="color: red">�No tienes aun ning�n grupo
              seleccionado!</span> <a href='<c:url value="/grupos"/>'>Seleccionar
              un grupo.</a>
          </h4>
        </div>
      </c:when>
      <c:when test="${listaMiembros != null }">
        <c:forEach items="${listaMiembros}" var="item">

          <div class="w3-code2">
            <h4>
              <c:out value="${item.nombre}"></c:out>
            </h4>
          </div>
        </c:forEach>

      </c:when>
    </c:choose>
    <br>
  </div>

  <%@include file="footer.jsp"%>
</body>
</html>