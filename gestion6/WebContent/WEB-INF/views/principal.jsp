
<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>

<body class="container">

<%@include file="navBar.jsp"%>
	
  <c:choose>
    <c:when test="${proyecto eq 'vacio' }">
	  <div>
	    <c:if test="${aviso ne null}">
		  <div class="alert alert-danger" role="alert" > ${aviso}</div>
		</c:if>
		<c:if test="${listaGrupos != null }">
		  <div class="panel panel-default">
  			<div class="panel-heading">
    		  <h3 class="panel-title">Selecciona un grupo:</h3>
  			</div>
  		    <div class="panel-body">
    		  <c:forEach items="${listaGrupos}" var="item">
    		    <form name='f' action="/gestion6/grupos/${item.idproyecto}" method='POST'>
    		  	  <ul>
				    <li><c:out value="${item.nombre}"></c:out>
				   	<input type="submit"  class="btn btn-success btn-xs pull-right" value="Seleccionar" /></li>				  
				  </ul> 
				</form>
			  </c:forEach>
  		    </div>
		  </div>
		</c:if>
	  </div>
    </c:when>
    <c:otherwise>
	  <br>
	  <div class="w3-code">
	    <h3 style="color:#1171C0;" >${proyecto} 		
	      <a href='<c:url value="/documentation"/>' role="button" title="Documentación" >
  	        <span class="glyphicon glyphicon-folder-open pull-right" aria-hidden="true" style="color:#222;"></span> 
	      </a>
	    </h3>
	  </div>
	  
	  <c:if test="${aviso ne null}">
	    <div class="alert" role="alert" > ${aviso}</div>
	  </c:if>
	 
	  <div class="w3-code2">
	    <h4>Requisitos:
	      <c:if test="${creador != null }">
	        <a href='<c:url value="/requisito/agregar"/>' role="button" title="Agregar requisito">
	          <span class="glyphicon glyphicon-plus pull-right" aria-hidden="true" style="color:#5cb85c;"></span> 
	        </a>
	      </c:if> 
	    </h4> 
	  </div>
	  <br>
	  <c:if test="${listaRequisitos != null}">
    
	    <div class="panel-group" id="accordion">
      
	      <c:forEach items="${listaRequisitos}" var="item">
        
  		    <div class="panel panel-default">
    	      <div class="panel-heading">
      		    <h4 class="panel-title">
        	      <a data-toggle="collapse" data-parent="#accordion" href="#collapse${item.idrequisito}">
                
        			<c:out value="${item.nombre}"></c:out>
                    <c:if test="${creador != null }">
                    
                      <a href='<c:url value="/requisito/borrar/${item.idrequisito}"/>' role="button" title="Borrar requisito">
                        <span class="glyphicon glyphicon-remove pull-right" aria-hidden="true" style="color:red;"></span> 
                      </a>
                      <a href='<c:url value="/requisito/actualizar/${item.idrequisito}"/>' role="button"  title="Actualizar requisito">
                        <span class="glyphicon glyphicon-refresh pull-right" aria-hidden="true" style="color:#222;">&nbsp</span> 
                      </a>
                      <a href='<c:url value="/tarea/agregar/${item.idrequisito}"/>' role="button" title="Agregar tarea">
                        <span class="glyphicon glyphicon-plus pull-right" aria-hidden="true" style="color:#5cb85c;">&nbsp</span> 
                      </a>
                      
                    </c:if>
         
                  </a>
      		    </h4>
    	      </div>
    	      <div id="collapse${item.idrequisito}" class="panel-collapse collapse">
      	        <div class="panel-body"> 
                
                  <c:if test="${fn:length(item.tareaList) == 0}">
                  
                    <span style="color: red">Este requisito aun no tiene tareas.</span>
                  
                  </c:if>
                  <c:forEach items="${item.tareaList}" var="item2">
                  
                    <h5>
                      <a href='<c:url value="/tarea/detalles/${item2.idtarea}"/>' role="button">
                        <c:out value="${item2.nombre}"></c:out>
                      </a>
                      <span class="badge" title="Plazas libres">
                        <c:out value="${item2.participantes - fn:length(item2.desarrolladorList)}"></c:out>
                      </span>
                      <c:choose>
                        <c:when test="${item2.verificado == 0 && fn:length(item2.desarrolladorList) == 0}">
                            <span class="glyphicon glyphicon-th-list pull-right" aria-hidden="true" style="color:red;" title="Libre!"></span>
                        </c:when>
                        <c:when test="${item2.verificado == 0 && fn:length(item2.desarrolladorList) > 0}">
                          <span class="glyphicon glyphicon-th-list pull-right" aria-hidden="true" style="color:orange;" title="En proceso"></span>
                        </c:when>
                        <c:when test="${item2.verificado == 1}">
                          <span class="glyphicon glyphicon-th-list pull-right" aria-hidden="true" style="color:green;" title="Finalizada"></span>
                        </c:when>
                      </c:choose>
                      
                      <c:if test="${creador != null }">
                        <a href='<c:url value="/tarea/borrar/${item2.idtarea}"/>' role="button">
                          <span class="glyphicon glyphicon-remove pull-right" aria-hidden="true" style="color:red;" title="Borrar tarea">&nbsp</span> 
                        </a>
                        <a href='<c:url value="/tarea/actualizar/${item2.idtarea}"/>' role="button">
                          <span class="glyphicon glyphicon-refresh pull-right" aria-hidden="true" style="color:#222;" title="Actualizar tarea">&nbsp</span> 
                        </a>
                      </c:if>
                      
                      <a href='<c:url value="/tarea/detalles/${item2.idtarea}"/>' role="button">
                        <span class="glyphicon glyphicon-zoom-in pull-right" aria-hidden="true" style="color:#222;" title="Ver detalles">&nbsp</span>
                      </a>
                      
                    </h5>
                  
                  </c:forEach>
                  
      	       </div>
    	      </div>
  	        </div>
  	      </c:forEach>
	    </div>
	  </c:if>
    </c:otherwise>
  </c:choose>

	
  <%@include file="footer.jsp"%>
</body>
</html>
