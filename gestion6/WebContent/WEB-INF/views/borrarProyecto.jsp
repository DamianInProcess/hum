<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>
<body class="container">
  <%@include file="navBar.jsp"%>

  <br>
  <div class="w3-code">
    <h3 style="color: #1171C0;">Grupos de los que te puedes dar de
      baja:</h3>
  </div>
  <br>
  <div>
    <c:choose>
      <c:when test="${listaGrupos == null }">

        <div class="w3-code2">
          <h4>
            <span style="color: red">No hay grupos que borrar.</span>
          </h4>
        </div>

      </c:when>
      <c:when test="${listaGrupos != null }">
        <c:forEach items="${listaGrupos}" var="item">
          <div class="w3-code2">
            <form name='f' action="borrar/${item.nombre}" method='POST'>
              <div class="input-group">
                <c:out value="${item.nombre}"></c:out>
                <span class="input-group-btn"> <input
                  class="btn btn-danger btn-xs" type="submit"
                  value="Borrar" /></span>
              </div>
            </form>
          </div>
          <br>
        </c:forEach>
      </c:when>
    </c:choose>
    <br>
  </div>

  <%@include file="footer.jsp"%>
</body>
</html>