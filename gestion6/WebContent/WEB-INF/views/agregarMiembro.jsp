<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>
<body class="container">

  <%@include file="navBar.jsp"%>
  <br>
  <div class="w3-code">
    <h3>Agregar miembros al grupo: ${proyecto}</h3>
  </div>
  <br>
  <c:if test="${error != null }">
    <div class="alert alert-danger" role="alert">${error}</div>
  </c:if>
  <br>
  <form name='agregarMiembro' action='agregarMiembro' method='POST'>

    <div class="form-group">
      <input  type="text" class="form-control" name='nombre' 
        placeholder="Nombre del usuario a invitar" />
    </div>
    <div class="pull-right">
      <button type="submit" class="btn btn-success">Enviar</button>
    </div>
  </form>
  <br>
  <div class="w3-code">
    <h3>Peticiones que est�n pendientes de contestar</h3>
  </div>
  <br>
  <div>
    <c:choose>
      <c:when test="${listaPendientes == null }">

        <div class="alert alert-danger" role="alert">No hay
          peticiones pendientes de contestar.</div>

      </c:when>
      <c:when test="${listaPendientes != null }">
        <c:forEach items="${listaPendientes}" var="item">
          <div class="w3-code2">
            <h5>
              <c:out value="${item.nombre}"></c:out>
            </h5>
          </div>
          <br>
        </c:forEach>
      </c:when>
    </c:choose>
  </div>

  <%@include file="footer.jsp"%>
</body>
</html>