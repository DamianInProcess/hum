<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>
<body class="container">

  <%@include file="navBar.jsp"%>

  <br>
  <div class="w3-code">
    <h3 style="color: #1171C0;">
      Grupos <a href="grupos/agregar" role="button"
        title="Agregar grupo"> <span
        class="glyphicon glyphicon-plus pull-right" aria-hidden="true"
        style="color: #5cb85c;"></span>
      </a> <a href="grupos/borrar" role="button" title="Borrar miembro">
        <span class="glyphicon glyphicon-remove pull-right"
        aria-hidden="true" style="color: red;">&nbsp</span>
      </a>
    </h3>
  </div>
  <br>

  <div>
    <c:choose>
      <c:when test="${listaGrupos == null }">
        <div class="alert alert-danger" role="alert">�No tienes
          aun ning�n grupo!</div>
      </c:when>
      
      <c:when test="${listaGrupos != null }">

        <c:forEach items="${listaGrupos}" var="item">
          <div class="w3-code2">
            <form name='f' action="grupos/${item.idproyecto}"
              method='POST'>
              <div class="input-group">
                <c:out value="${item.nombre}"></c:out>
                <span class="input-group-btn"> <input
                  class="btn btn-success btn-xs" type="submit"
                  value="Seleccionar" /></span>
              </div>
            </form>
          </div>
          <br>
        </c:forEach>
      </c:when>
    </c:choose>
  </div>
  
  <c:if test="${creador}">
    <a href="grupos/actualizar">actualizar</a>
  </c:if><!-- funcionalidad que falta por implementar -->

  <%@include file="footer.jsp"%>
</body>
</html>