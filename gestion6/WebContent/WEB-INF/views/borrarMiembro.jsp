<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>
<body class="container">
  <%@include file="navBar.jsp"%>

  <br>
  <div class="w3-code">
    <h3 style="color: #1171C0;">Eliminar miembro del grupo:
      ${proyecto}</h3>
  </div>
  <br>

  <c:if test="${error != null }">
    <div class="alert alert-danger" role="alert">${error}</div>
    <br>
  </c:if>
  <c:if test="${exito != null }">
    <div class="alert alert-danger" role="alert">Se ha eliminado
      al usuario correctamente del grupo</div>
    <br>
  </c:if>

  <div>
    <c:choose>
      <c:when test="${listaMiembros == null }">

        <div class="alert alert-danger" role="alert">No se ha
          seleccionado grupo!</div>


      </c:when>
      <c:when test="${listaMiembros != null }">

        <c:forEach items="${listaMiembros}" var="item">
            <div class="w3-code2"> 
              <form name='borrarMiembro' action='borrar/${item.nombre}'
                method='POST'>
                <div class="input-group">
                  <c:out value="${item.nombre}"></c:out>
                  <span class="input-group-btn"> <input
                    class="btn btn-danger btn-xs" type="submit"
                    value="Seleccionar" /></span>
                </div>
              </form>
            </div>
            <br>
        </c:forEach>

        <c:if test="${fn:length(listaMiembros) == 0}">
          <div class="alert alert-danger" role="alert">No hay
            miembros que poder eliminar.</div>
        </c:if>
      </c:when>
    </c:choose>
    <br>
  </div>

  <%@include file="footer.jsp"%>
</body>
</html>