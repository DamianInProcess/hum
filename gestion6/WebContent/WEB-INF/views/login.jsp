
<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>
<body class="container">

  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed"
          data-toggle="collapse" data-target="#login"
          aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Barra de navegaci�n</span> <span
            class="icon-bar"></span> <span class="icon-bar"></span> <span
            class="icon-bar"></span>
        </button>

        <!-- tendr� que ir el logo aqu�  -->

        <a class="navbar-brand" href="#"><img alt="Home" src='<c:url value="/res/image/HUM.png" />'></a>
      </div>

      <div id="login" class="navbar-collapse collapse">

        <form class="navbar-form navbar-right" id="f" name='f'
          action="login" method='POST'>
          <div class="form-group">
            <input type="text" class="form-control"
              placeholder="Usuario" name='username'>
          </div>
          <div class="form-group">
            <input type="password" placeholder="Contrase�a"
              class="form-control" name='password'>
          </div>
          <button type="submit" class="btn btn-success">Aceptar</button>
        </form>
      </div>
      <!--/.navbar-collapse -->
    </div>
  </nav>

  <div class="well well-sm">
    <div class="container">
      <c:choose>
        <c:when test="${param.error != null }">
          <p>�Usuario o contrase�a incorrecta!</p>
        </c:when>
        <c:when test="${exito != null }">
          <p>�Te has registrado con exito!</p>
        </c:when>
        <c:when test="${error != null }">
          <p>${error}</p>
        </c:when>
        <c:otherwise>
          <p>Bienvenido.</p>
        </c:otherwise>
      </c:choose>

    </div>
  </div>

  <div class="row">
    <h1 class="text-center">�Aun no eres usuari@? Registrate!</h1>
    <div class="col-md-4 col-md-offset-4">
      <sf:form action="desarrollador" method="POST"
        modelAttribute="desa">
        <div class="form-group">
          <sf:input path="nombre" type="text" class="form-control"
            placeholder="Usuario" />
          <sf:errors path="nombre" cssStyle="color:red" />
        </div>
        <div class="form-group">
          <sf:input path="password" type="password"
            placeholder="Contrase�a" class="form-control" />
          <sf:errors path="password" cssStyle="color:red" />
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-success">Aceptar</button>
        </div>
      </sf:form>
    </div>
  </div>

  <%@include file="footer.jsp"%>

</body>
</html>