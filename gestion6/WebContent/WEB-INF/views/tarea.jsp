<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>
<body class="container">

  <%@include file="navBar.jsp"%>

  <div class="blog-header">
    <h1 class="blog-title">${requisito.nombre}
      <c:if test="${participantes ne true}">
        <c:if
          test="${fn:length(tarea.desarrolladorList) < tarea.participantes }">
          <a href='<c:url value="participar/${tarea.idtarea}"/>'
            role="button" title="Ap�ntate!"> <span
            class="btn btn-success btn-xl pull-right" aria-hidden="true">Ap�ntate!</span>
          </a>
        </c:if>
      </c:if>
      <c:if test="${participantes eq true and tarea.verificado eq 0}">
        <a href='<c:url value="abandonar/${tarea.idtarea}"/>'
          role="button" title="Ap�ntate!"> <span
          class="btn btn-danger btn-xl pull-right" aria-hidden="true">Abandonar</span>
        </a>
      </c:if>
    </h1>
    <c:if test="${requisito.descripcion != null }">
      <p class="lead blog-description">${requisito.descripcion}</p>
    </c:if>
  </div>

  <div class="row">
    <div class="col-sm-8 blog-main">
      <div class="blog-post">
        <h2 class="blog-post-title">${tarea.nombre}</h2>
        <p>${tarea.descripcion}</p>
        <c:if test="${tarea.comentarios ne null}">
          <blockquote>
            <p>${tarea.comentarios}</p>
          </blockquote>
        </c:if>
        <c:if test="${listaImagenes ne null }">
          <div class="album text-muted">
            <div class="container">
              <div class="row col-sm-8">
                <c:forEach items="${listaImagenes}" var="item">

                  <div class="card">
                    <a target='_blank'
                      href="data:image/jpg;base64, ${item.imagen}">
                      <img id="myImg" 
                      src="data:image/jpg;base64, ${item.imagen}"
                      alt="${item.nombre} not found">
                    </a>
                    <p class="card-text">${item.nombre}
                      <a
                        href="<c:url value='borrarImagen/${item.idimagenTarea}/${tarea.idtarea}'/>">borrar</a>
                    </p>
                  </div>
                </c:forEach>
              </div>
            </div>
          </div>
        </c:if>
      </div>

      <div>
        <c:if test="${participantes eq true}">
          <form action="imagenTarea/${tarea.idtarea}"
            enctype="multipart/form-data" method="post">
            <div class="input-group">
              <input type="file" name="pic" accept="image/*"> <span
                class="input-group-btn">
                <button type="submit" class="btn btn-success">Subir</button>
              </span>
            </div>
          </form>
          <br>
          <form action="comit/${tarea.idtarea}" method="post">
            <div class="input-group">
              <input name='comit' type="text" class="form-control"
                placeholder="Objetivos Cumplidos"> <span
                class="input-group-btn">
                <button type="submit" class="btn btn-success">Aceptar</button>
              </span>
            </div>
          </form>
          <br>
          <sf:form action="comentarios/${requisito.idrequisito}"
            modelAttribute="tarea">
            <div class="form-group">
              <sf:textarea path="comentarios" class="form-control"
                rows="5" placeholder="Comentario..." />
              <sf:errors path="comentarios" cssStyle="color:red" />
            </div>
            <div class="form-group text-center">
              <label class="radio-inline"> <sf:radiobutton
                  path="verificado" value="0" /> Aun no...
              </label> <label class="radio-inline"> <sf:radiobutton
                  path="verificado" value="1" /> Finaliazada
              </label>
            </div>
            <div>
              <sf:input type="hidden" path="nombre" />
              <sf:input type="hidden" path="descripcion" />
              <sf:input type="hidden" path="esfuerzo" />
              <sf:input type="hidden" path="idtarea" />
            </div>
            <div class="form-group pull-right">
              <button type="submit" class="btn btn-success">Actualizar</button>
            </div>
          </sf:form>
        </c:if>
      </div>
      <br>
    </div>
    <!-- /.blog-main -->

    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
      <div class="sidebar-module sidebar-module-inset">
        <h5>
          Esfuerzo:<b> ${tarea.esfuerzo} </b>
        </h5>
        <h5>
          Max personas:<b> ${tarea.participantes} </b>
        </h5>
        <h5>Personas inscritas:</h5>
        <c:if test="${fn:length(tarea.desarrolladorList) > 0}">
          <c:forEach items="${tarea.desarrolladorList}" var="item">
            <h5>
              <b><c:out value="${item.nombre}"></c:out></b>
              <c:if test="${creador ne null }">
                <a
                  href='<c:url value="borrar/${item.iddesarrollador}/${tarea.idtarea}"/>'
                  role="button" title="Quitar participante"> <span
                  class="glyphicon glyphicon-remove pull-right"
                  aria-hidden="true" style="color: red;"></span>
                </a>
              </c:if>
            </h5>
          </c:forEach>
        </c:if>
        <div class="sidebar-module">
          <h4>Objetivos Cumplidos:</h4>
          <ol class="list-unstyled">
            <c:if test="${listaComits ne null}">
              <c:forEach items="${listaComits}" var="item">
                <li><p>${item.comit}</p></li>
              </c:forEach>
            </c:if>
          </ol>
        </div>
      </div>
    </div>
    <!-- /.blog-sidebar -->
  </div>
  <!-- /.row -->

  <%@include file="footer.jsp"%>

</body>
</html>
