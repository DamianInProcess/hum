<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>
<body class="container">

  <%@include file="navBar.jsp"%>
  <br>
  <sf:form action="agregar" method="POST"
    modelAttribute="requisitoModelo">
    <div class="form-group">
      <sf:input path="nombre" type="text" class="form-control"
        placeholder="�Qu� nombre tendr� el requisito?" />
      <sf:errors path="nombre" cssStyle="color:red" />
    </div>
    <div class="form-group">
      <sf:input path="descripcion" type="text" class="form-control"
        placeholder="Una descripci�n que ayude a llevar a cabo la tarea" />
      <sf:errors path="descripcion" cssStyle="color:red" />
    </div>
    <div class="pull-right">
      <button type="submit" class="btn btn-success">Aceptar</button>
    </div>
  </sf:form>
  <br>

  <%@include file="footer.jsp"%>
</body>
</html>