<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>
<body class="container">

  <%@include file="navBar.jsp"%>

  <br>
  <div class="w3-code">
    <h3 style="color: #1171C0;">Peticiones pendientes</h3>
  </div>
  <br>
  <div>
    <c:choose>
      <c:when test="${listaPeticiones == null }">

        <div class="alert alert-danger" role="alert">No tienes
          peticiones pendientes.</div>

      </c:when>
      <c:when test="${listaPeticiones != null }">
        <c:forEach items="${listaPeticiones}" var="item">

          <div class="w3-code2">
            <h4>
              <c:out value="${item.nombre}"></c:out>
              <a
                href='<c:url value="/peticiones/aceptar/${item.nombre}"/>'
                role="button" title="Aceptar petici�n"> <span
                class="glyphicon glyphicon-ok pull-right"
                aria-hidden="true" style="color: #5cb85c;"></span>
              </a> <a
                href='<c:url value="/peticiones/rechazar/${item.nombre}"/>'
                role="button" title="Rechazar petici�n"> <span
                class="glyphicon glyphicon-remove pull-right"
                aria-hidden="true" style="color: red;">&nbsp</span>
              </a>
            </h4>
          </div>
          <br>
        </c:forEach>
      </c:when>
    </c:choose>
  </div>

  <%@include file="footer.jsp"%>

</body>
</html>