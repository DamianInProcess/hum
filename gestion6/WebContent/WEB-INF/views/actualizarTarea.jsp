
<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>

</head>
<body class="container">
  <%@include file="navBar.jsp"%>
  <br>
  <sf:form action="${id}" method="POST" modelAttribute="tareaModelo">
    <div class="form-group">
      <sf:input path="nombre" type="text" class="form-control"
        placeholder="�Qu� nombre tendr� la tarea?" />
      <sf:errors path="nombre" cssStyle="color:red" />
    </div>
    <div class="form-group">
      <sf:input path="descripcion" type="text-area" class="form-control"
        placeholder="descripci�n que ayude a llevar a cabo la tarea" />
      <sf:errors path="descripcion" cssStyle="color:red" />
    </div>
    <div class="form-group">
      <sf:input path="comentarios" type="text-area" class="form-control"
        placeholder="Comentario" />
      <sf:errors path="comentarios" cssStyle="color:red" />
    </div>
    <div class="form-group">
      <sf:input path="esfuerzo" type="text" class="form-control"
        placeholder="�Qu� cantidad de esfuerzo o de tiempo crees que se necesitar� para llevar a cabo esta tarea?" />
      <sf:errors path="esfuerzo" cssStyle="color:red" />
    </div>
    <div class="form-group">
      <sf:input path="participantes" type="text" class="form-control"
        placeholder="�Qu� n�mero de participantes tendr� esta tarea?"
        value="1" />
      <sf:errors path="participantes" cssStyle="color:red" />
    </div>
    <sf:input type="hidden" path="verificado" />
    <sf:input type="hidden" path="idtarea" />
    <div class="pull-right">
      <button type="submit" class="btn btn-success">Aceptar</button>
    </div>
  </sf:form>
  <br>

  <%@include file="footer.jsp"%>
</body>
</html>