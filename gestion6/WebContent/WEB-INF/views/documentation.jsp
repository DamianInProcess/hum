<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="head.jsp"%>
<title>Hum</title>
</head>

<body class="container">
  <%@include file="navBar.jsp"%>
  <div class="row">
    <c:if test="${listaImagenes ne null }">
      <div class="album text-muted">
        <div class="container">
          <div class="row col-sm-8">
            <c:forEach items="${listaImagenes}" var="item">
              <div class="card">
                <a target='_blank'
                  href="data:image/jpg;base64, ${item.imagen}"> <img
                  id="myImg" src="data:image/jpg;base64, ${item.imagen}"
                  alt="${item.nombre} not found">
                </a>
                <p class="card-text">${item.nombre}
                  <a
                    href="<c:url value='borrarImagen/${item.idimagenTarea}/${tarea.idtarea}'/>">borrar</a>
                </p>
              </div>
            </c:forEach>
          </div>
        </div>
      </div>
    </c:if>
  </div>
  
  <div>
    <form action="imagenProyecto"
      enctype="multipart/form-data" method="post">
      <div class="input-group">
        <input type="file" name="pic" accept="image/*"> <span
          class="input-group-btn">
          <button type="submit" class="btn btn-success">Subir</button>
        </span>
      </div>
    </form>
  </div>
  <%@include file="footer.jsp"%>
</body>

</html>