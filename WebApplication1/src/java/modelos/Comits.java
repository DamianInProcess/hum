/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Damian
 */
@Entity
@Table(name = "comits")
@NamedQueries({
    @NamedQuery(name = "Comits.findAll", query = "SELECT c FROM Comits c")})
public class Comits implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcomits")
    private Integer idcomits;
    @Column(name = "comit")
    private String comit;
    @JoinColumn(name = "idtarea", referencedColumnName = "idtarea")
    @ManyToOne(optional = false)
    private Tarea idtarea;

    public Comits() {
    }

    public Comits(Integer idcomits) {
        this.idcomits = idcomits;
    }

    public Integer getIdcomits() {
        return idcomits;
    }

    public void setIdcomits(Integer idcomits) {
        this.idcomits = idcomits;
    }

    public String getComit() {
        return comit;
    }

    public void setComit(String comit) {
        this.comit = comit;
    }

    public Tarea getIdtarea() {
        return idtarea;
    }

    public void setIdtarea(Tarea idtarea) {
        this.idtarea = idtarea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcomits != null ? idcomits.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comits)) {
            return false;
        }
        Comits other = (Comits) object;
        if ((this.idcomits == null && other.idcomits != null) || (this.idcomits != null && !this.idcomits.equals(other.idcomits))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelos.Comits[ idcomits=" + idcomits + " ]";
    }
    
}
