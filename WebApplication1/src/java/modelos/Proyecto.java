/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Damian
 */
@Entity
@Table(name = "proyecto")
@NamedQueries({
    @NamedQuery(name = "Proyecto.findAll", query = "SELECT p FROM Proyecto p")})
public class Proyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idproyecto")
    private Integer idproyecto;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @ManyToMany(mappedBy = "proyectoList")
    private List<Desarrollador> desarrolladorList;
    @ManyToMany(mappedBy = "proyectoList")
    private List<ImagenProyecto> imagenProyectoList;
    @ManyToMany(mappedBy = "proyectoList1")
    private List<Desarrollador> desarrolladorList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproyecto")
    private List<Requisito> requisitoList;
    @JoinColumn(name = "iddesarrollador", referencedColumnName = "iddesarrollador")
    @ManyToOne(optional = false)
    private Desarrollador iddesarrollador;

    public Proyecto() {
    }

    public Proyecto(Integer idproyecto) {
        this.idproyecto = idproyecto;
    }

    public Proyecto(Integer idproyecto, String nombre) {
        this.idproyecto = idproyecto;
        this.nombre = nombre;
    }

    public Integer getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Integer idproyecto) {
        this.idproyecto = idproyecto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Desarrollador> getDesarrolladorList() {
        return desarrolladorList;
    }

    public void setDesarrolladorList(List<Desarrollador> desarrolladorList) {
        this.desarrolladorList = desarrolladorList;
    }

    public List<ImagenProyecto> getImagenProyectoList() {
        return imagenProyectoList;
    }

    public void setImagenProyectoList(List<ImagenProyecto> imagenProyectoList) {
        this.imagenProyectoList = imagenProyectoList;
    }

    public List<Desarrollador> getDesarrolladorList1() {
        return desarrolladorList1;
    }

    public void setDesarrolladorList1(List<Desarrollador> desarrolladorList1) {
        this.desarrolladorList1 = desarrolladorList1;
    }

    public List<Requisito> getRequisitoList() {
        return requisitoList;
    }

    public void setRequisitoList(List<Requisito> requisitoList) {
        this.requisitoList = requisitoList;
    }

    public Desarrollador getIddesarrollador() {
        return iddesarrollador;
    }

    public void setIddesarrollador(Desarrollador iddesarrollador) {
        this.iddesarrollador = iddesarrollador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproyecto != null ? idproyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto)) {
            return false;
        }
        Proyecto other = (Proyecto) object;
        if ((this.idproyecto == null && other.idproyecto != null) || (this.idproyecto != null && !this.idproyecto.equals(other.idproyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelos.Proyecto[ idproyecto=" + idproyecto + " ]";
    }
    
}
