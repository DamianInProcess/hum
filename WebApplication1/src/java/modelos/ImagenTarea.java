/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Damian
 */
@Entity
@Table(name = "imagenTarea")
@NamedQueries({
    @NamedQuery(name = "ImagenTarea.findAll", query = "SELECT i FROM ImagenTarea i")})
public class ImagenTarea implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idimagenTarea")
    private Integer idimagenTarea;
    @Basic(optional = false)
    @Lob
    @Column(name = "imagen")
    private byte[] imagen;
    @JoinTable(name = "tarea_imagentarea", joinColumns = {
        @JoinColumn(name = "idimagenTarea", referencedColumnName = "idimagenTarea")}, inverseJoinColumns = {
        @JoinColumn(name = "idtarea", referencedColumnName = "idtarea")})
    @ManyToMany
    private List<Tarea> tareaList;

    public ImagenTarea() {
    }

    public ImagenTarea(Integer idimagenTarea) {
        this.idimagenTarea = idimagenTarea;
    }

    public ImagenTarea(Integer idimagenTarea, byte[] imagen) {
        this.idimagenTarea = idimagenTarea;
        this.imagen = imagen;
    }

    public Integer getIdimagenTarea() {
        return idimagenTarea;
    }

    public void setIdimagenTarea(Integer idimagenTarea) {
        this.idimagenTarea = idimagenTarea;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public List<Tarea> getTareaList() {
        return tareaList;
    }

    public void setTareaList(List<Tarea> tareaList) {
        this.tareaList = tareaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idimagenTarea != null ? idimagenTarea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImagenTarea)) {
            return false;
        }
        ImagenTarea other = (ImagenTarea) object;
        if ((this.idimagenTarea == null && other.idimagenTarea != null) || (this.idimagenTarea != null && !this.idimagenTarea.equals(other.idimagenTarea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelos.ImagenTarea[ idimagenTarea=" + idimagenTarea + " ]";
    }
    
}
