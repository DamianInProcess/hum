/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Damian
 */
@Entity
@Table(name = "imagenProyecto")
@NamedQueries({
    @NamedQuery(name = "ImagenProyecto.findAll", query = "SELECT i FROM ImagenProyecto i")})
public class ImagenProyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idimagenProyecto")
    private Integer idimagenProyecto;
    @Basic(optional = false)
    @Lob
    @Column(name = "imagen")
    private byte[] imagen;
    @JoinTable(name = "proyecto_imagenproyecto", joinColumns = {
        @JoinColumn(name = "idimagenproyecto", referencedColumnName = "idimagenProyecto")}, inverseJoinColumns = {
        @JoinColumn(name = "idproyecto", referencedColumnName = "idproyecto")})
    @ManyToMany
    private List<Proyecto> proyectoList;

    public ImagenProyecto() {
    }

    public ImagenProyecto(Integer idimagenProyecto) {
        this.idimagenProyecto = idimagenProyecto;
    }

    public ImagenProyecto(Integer idimagenProyecto, byte[] imagen) {
        this.idimagenProyecto = idimagenProyecto;
        this.imagen = imagen;
    }

    public Integer getIdimagenProyecto() {
        return idimagenProyecto;
    }

    public void setIdimagenProyecto(Integer idimagenProyecto) {
        this.idimagenProyecto = idimagenProyecto;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public List<Proyecto> getProyectoList() {
        return proyectoList;
    }

    public void setProyectoList(List<Proyecto> proyectoList) {
        this.proyectoList = proyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idimagenProyecto != null ? idimagenProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImagenProyecto)) {
            return false;
        }
        ImagenProyecto other = (ImagenProyecto) object;
        if ((this.idimagenProyecto == null && other.idimagenProyecto != null) || (this.idimagenProyecto != null && !this.idimagenProyecto.equals(other.idimagenProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelos.ImagenProyecto[ idimagenProyecto=" + idimagenProyecto + " ]";
    }
    
}
