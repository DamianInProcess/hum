/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Damian
 */
@Entity
@Table(name = "tarea")
@NamedQueries({
    @NamedQuery(name = "Tarea.findAll", query = "SELECT t FROM Tarea t")})
public class Tarea implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtarea")
    private Integer idtarea;
    @Basic(optional = false)
    @Column(name = "participantes")
    private int participantes;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "esfuerzo")
    private Float esfuerzo;
    @Basic(optional = false)
    @Column(name = "verificado")
    private String verificado;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "comentarios")
    private String comentarios;
    @ManyToMany(mappedBy = "tareaList")
    private List<ImagenTarea> imagenTareaList;
    @ManyToMany(mappedBy = "tareaList")
    private List<Desarrollador> desarrolladorList;
    @JoinColumn(name = "idrequisito", referencedColumnName = "idrequisito")
    @ManyToOne(optional = false)
    private Requisito idrequisito;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtarea")
    private List<Comits> comitsList;

    public Tarea() {
    }

    public Tarea(Integer idtarea) {
        this.idtarea = idtarea;
    }

    public Tarea(Integer idtarea, int participantes, String verificado, String nombre) {
        this.idtarea = idtarea;
        this.participantes = participantes;
        this.verificado = verificado;
        this.nombre = nombre;
    }

    public Integer getIdtarea() {
        return idtarea;
    }

    public void setIdtarea(Integer idtarea) {
        this.idtarea = idtarea;
    }

    public int getParticipantes() {
        return participantes;
    }

    public void setParticipantes(int participantes) {
        this.participantes = participantes;
    }

    public Float getEsfuerzo() {
        return esfuerzo;
    }

    public void setEsfuerzo(Float esfuerzo) {
        this.esfuerzo = esfuerzo;
    }

    public String getVerificado() {
        return verificado;
    }

    public void setVerificado(String verificado) {
        this.verificado = verificado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public List<ImagenTarea> getImagenTareaList() {
        return imagenTareaList;
    }

    public void setImagenTareaList(List<ImagenTarea> imagenTareaList) {
        this.imagenTareaList = imagenTareaList;
    }

    public List<Desarrollador> getDesarrolladorList() {
        return desarrolladorList;
    }

    public void setDesarrolladorList(List<Desarrollador> desarrolladorList) {
        this.desarrolladorList = desarrolladorList;
    }

    public Requisito getIdrequisito() {
        return idrequisito;
    }

    public void setIdrequisito(Requisito idrequisito) {
        this.idrequisito = idrequisito;
    }

    public List<Comits> getComitsList() {
        return comitsList;
    }

    public void setComitsList(List<Comits> comitsList) {
        this.comitsList = comitsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtarea != null ? idtarea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarea)) {
            return false;
        }
        Tarea other = (Tarea) object;
        if ((this.idtarea == null && other.idtarea != null) || (this.idtarea != null && !this.idtarea.equals(other.idtarea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelos.Tarea[ idtarea=" + idtarea + " ]";
    }
    
}
